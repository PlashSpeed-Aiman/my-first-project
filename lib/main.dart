import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:kict_support_app/auth/google_auth.dart';
import 'package:kict_support_app/screen/auth_screen.dart';
import 'package:kict_support_app/screen/home_screen.dart';
import 'package:kict_support_app/screen/wrapper.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return GoogleSignInService();
      },
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.blue[500],
          canvasColor: Colors.white,
          ),
          //scaffoldBackgroundColor: Color(0xFFFFDFDF)),
        title: 'Material App',
        home: Wrapper(),
      ),
    );
  }
}
