import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kict_support_app/auth/google_auth.dart';
import 'package:kict_support_app/constant/screen_config.dart';
import 'package:provider/provider.dart';
import 'package:simple_shadow/simple_shadow.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Color(0xFFFFDFDF),
        elevation: 0,
      ),
      body: Container(
        child: Center(
          child: Column(
            children: [
              Expanded(
                child: Column(
                  children: [
                    SimpleShadow(
                      child: Image.asset(
                        'assets/images/logo.png',
                        width: getProportionateScreenWidth(150),
                        height: getProportionateScreenHeight(150),
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      'IT Support',
                      style: TextStyle(
                        fontSize: getProportionateScreenHeight(30),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, right: 20.0),
                                  child: Divider(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                              Text('Sign in with Google'),
                              Expanded(
                                child: Container(
                                  margin:
                                      EdgeInsets.only(left: 20.0, right: 20.0),
                                  child: Divider(
                                    color: Colors.grey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(15),
                          ),
                          MaterialButton(
                            shape: StadiumBorder(),
                            color: Color(0XFFEFEFEF),
                            hoverColor: Colors.transparent,
                            elevation: 3,
                            onPressed: () {
                              final provider = Provider.of<GoogleSignInService>(
                                  context,
                                  listen: false);

                              provider.googleLogin();
                            },
                            child: Container(
                              height: getProportionateScreenHeight(45),
                              width: getProportionateScreenWidth(255),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    'assets/svg/google.svg',
                                    width: getProportionateScreenWidth(23),
                                    height: getProportionateScreenHeight(23),
                                  ),
                                  SizedBox(width: 10),
                                  Text(
                                    'Google',
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
