import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kict_support_app/screen/login_screen.dart';
import 'package:kict_support_app/screen/wrapper.dart';

class AuthScreen extends StatelessWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: FirebaseAuth.instance.authStateChanges(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            print('wait a second');
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            print('Something went wrong!');
            return LoginScreen();
          } else if (snapshot.hasData) {
            print(snapshot.connectionState);
            return Wrapper();
          } else {
            return LoginScreen();
          }
        },
      ),
    );
  }
}
