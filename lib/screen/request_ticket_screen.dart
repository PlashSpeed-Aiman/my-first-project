import 'dart:html';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
class RequestScreen extends StatefulWidget {
  @override
  _RequestScreenState createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {

  final userRef = FirebaseFirestore.instance.collection('KOE/ticket/TicketID');

  String _dropDownVenue = 'Select';
  String _dropDownProblem = 'Select';

  final _descriptionController = TextEditingController();

  addTicket(String _description, String _venue, String _problem) {
    FirebaseFirestore.instance.collection('KOE/ticket/TicketID').doc().set({

      'Venue' : _venue,
      'Problems' : _problem,
      'Description' : _description

    });            
  }


  getUsers()  async{
    final String id = 'LAB C';
    final DocumentSnapshot doc = await userRef.doc(id).get();
    print(doc.data());
      
    }

  void initState () {
      getUsers();
      super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.navigate_before),
        ),
        title: Container(
          margin: EdgeInsets.fromLTRB(100, 10, 10, 10),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/logo.png',
                width: 30,
                height: 30,
              ),
              SizedBox(
                width: 10,
              ),
              Text('IT Support',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
            ],
          ),
        ),
      ),
      //backgroundColor: Colors.blueGrey,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(20, 60, 20, 20),
                width: 340,
                height: 130,
                decoration: BoxDecoration(
                  color: Colors.blue[100],
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                          //borderRadius: BorderRadius.circular(12),
                          color: Colors.blue[600]),
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Choose the venue :',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      width: 120,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.blue[300],
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              value: _dropDownVenue,
                              icon: Icon(Icons.arrow_drop_down_sharp),
                              iconSize: 24,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  _dropDownVenue = newValue!;
                                });
                              },
                              items: <String>[
                                'Select',
                                'One',
                                'Two',
                                'Three'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList()),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                width: 340,
                height: 130,
                decoration: BoxDecoration(
                    color: Colors.blue[100],
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                        //borderRadius: BorderRadius.circular(12),
                        color: Colors.blue[600],
                      ),
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Problems :',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      width: 200,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.blue[300],
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: Center(
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              value: _dropDownProblem,
                              icon: Icon(Icons.arrow_drop_down_sharp),
                              iconSize: 24,
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  _dropDownProblem = newValue!;
                                });
                              },
                              items: <String>[
                                'Select',
                                'Problem 1',
                                'Problem 2',
                                'Problem 3'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(
                                    value,
                                    textAlign: TextAlign.center,
                                  ),
                                );
                              }).toList()),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                width: 340,
                height: 150,
                decoration: BoxDecoration(
                  color: Colors.blue[100],
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(
                  children: [
                    Container(
                      width: double.infinity,
                      height: 40,
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 5),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Description :',
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    Container(
                      width: 200,
                      height: 30,
                      child: TextFormField(
                        controller: _descriptionController,
                        decoration: InputDecoration(
                          fillColor: Colors.blue,
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextButton(
                      
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.blue[300],
                      ),
                      onPressed: () {
                        addTicket(_descriptionController.text, _dropDownVenue, _dropDownProblem);
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
